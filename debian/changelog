cmark-gfm (0.29.0.gfm.6-6+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 06 Apr 2023 09:36:29 +0000

cmark-gfm (0.29.0.gfm.6-6) unstable; urgency=medium

  * Another attempt to get spec.html to be created by buildd

 -- Keith Packard <keithp@keithp.com>  Wed, 28 Dec 2022 22:21:05 -0800

cmark-gfm (0.29.0.gfm.6-5) unstable; urgency=medium

  * Merge in patch from NMU 0.29.0.gfm.6-2.1

 -- Keith Packard <keithp@keithp.com>  Wed, 28 Dec 2022 21:38:59 -0800

cmark-gfm (0.29.0.gfm.6-4) unstable; urgency=medium

  * Add install depend on spec.html to get that built at the right time

 -- Keith Packard <keithp@keithp.com>  Wed, 28 Dec 2022 21:31:22 -0800

cmark-gfm (0.29.0.gfm.6-3) unstable; urgency=medium

  * Set cmark-gfm to Multi-Arch: foriegn. (Closes: #1027152)
  * Install config.h with libcmark-gfm-dev. (Closes: #1022183)
  * Install spec (both .txt and .html). (Closes: #1023350)

 -- Keith Packard <keithp@keithp.com>  Wed, 28 Dec 2022 13:41:28 -0800

cmark-gfm (0.29.0.gfm.6-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Apply patch to fix FTBFS on s390x (Closes: #1023020)

 -- Scott Talbert <swt@techie.net>  Wed, 30 Nov 2022 10:41:13 -0500

cmark-gfm (0.29.0.gfm.6-2) unstable; urgency=medium

  * Binary version because the library always changes package
    names.
  * New upstream includes fix for CVE-2022-39209. (Closes: #1020588)
  
 -- Keith Packard <keithp@keithp.com>  Tue, 25 Oct 2022 23:24:33 -0700

cmark-gfm (0.29.0.gfm.6-1) unstable; urgency=medium

  * New upstream version.
  * Install all headers in /usr/include/cmark-gfm. (Closes: #1022183)

 -- Keith Packard <keithp@keithp.com>  Tue, 25 Oct 2022 22:38:21 -0700

cmark-gfm (0.29.0.gfm.3-3) unstable; urgency=medium

  * Need to upload orig source

 -- Keith Packard <keithp@keithp.com>  Fri, 11 Mar 2022 12:10:24 -0800

cmark-gfm (0.29.0.gfm.3-2) unstable; urgency=medium

  * Need to upload binary version

 -- Keith Packard <keithp@keithp.com>  Fri, 11 Mar 2022 11:15:14 -0800

cmark-gfm (0.29.0.gfm.3-1) unstable; urgency=medium

  * New upstream version.
  * Fix for CVE-2022-24724. (Closes: #1006756)

 -- Keith Packard <keithp@keithp.com>  Fri, 11 Mar 2022 10:39:54 -0800

cmark-gfm (0.29.0.gfm.2-2) unstable; urgency=medium

  * Generate library package names from cmark-gfm upstream
    version. (Closes: #1003964)

 -- Keith Packard <keithp@keithp.com>  Wed, 19 Jan 2022 16:03:13 -0800

cmark-gfm (0.29.0.gfm.2-1) unstable; urgency=medium

  * New upstream version.
  * Fix for CVE-2020-5238. (Closes: #965984)
  * Switch man page --safe to --unsafe. (Closes: #1003745)

 -- Keith Packard <keithp@keithp.com>  Mon, 17 Jan 2022 16:43:27 -0800

cmark-gfm (0.29.0.gfm.0-6apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Tue, 20 Apr 2021 13:12:22 +0530

cmark-gfm (0.29.0.gfm.0-6) unstable; urgency=medium

  * Use <stdbool.h> directly instead of "config.h". (Closes: #969212)

 -- Keith Packard <keithp@keithp.com>  Wed, 02 Sep 2020 10:25:33 -0700

cmark-gfm (0.29.0.gfm.0-5) unstable; urgency=medium

  * Install header files in -dev packages. (Closes: #968709)
  * Fix .symbols files for new upstream version.
  * Install libcmark-gfm.pc in arch directory

 -- Keith Packard <keithp@keithp.com>  Fri, 28 Aug 2020 10:24:17 -0700

cmark-gfm (0.29.0.gfm.0-4) unstable; urgency=medium

  * Add patch to make tests compatible with Python 3.8

 -- Keith Packard <keithp@keithp.com>  Mon, 09 Mar 2020 13:16:13 -0700

cmark-gfm (0.29.0.gfm.0-3) unstable; urgency=medium

  * Add patch increasing pathological test limit to 10s for sparc64

 -- Keith Packard <keithp@keithp.com>  Tue, 17 Dec 2019 13:33:53 -0800

cmark-gfm (0.29.0.gfm.0-2) unstable; urgency=medium

  * Merge upstream fix for tasklist on MSB machines (Closes: #944652)

 -- Keith Packard <keithp@keithp.com>  Mon, 16 Dec 2019 20:37:41 -0800

cmark-gfm (0.29.0.gfm.0-1) unstable; urgency=medium

  * Upstream version 0.29.0.gfm.0
  * Change 'debian/watch' to look for upstream tags including 'gfm'
  * Replace large file support for 32-bit builds with upstream version

 -- Keith Packard <keithp@keithp.com>  Mon, 22 Apr 2019 11:26:50 -0700

cmark-gfm (0.28.3.gfm.20-3) unstable; urgency=medium

  * Mark libraries as Multi-Arch: same

 -- Keith Packard <keithp@keithp.com>  Sat, 09 Mar 2019 19:23:07 +0100

cmark-gfm (0.28.3.gfm.20-2) unstable; urgency=medium

  * Change debian/watch to monitor git tags instead of tar.gz files

 -- Keith Packard <keithp@keithp.com>  Sat, 09 Mar 2019 17:00:52 +0100

cmark-gfm (0.28.3.gfm.20-1) unstable; urgency=medium

  * Upstream version 0.28.3.gfm.20
  * Mark library dependency bug closed. (Closes: #911049)
  * Add LFS support.

 -- Keith Packard <keithp@keithp.com>  Sat, 09 Mar 2019 11:29:52 +0100

cmark-gfm (0.28.3.gfm.19-3) unstable; urgency=medium

  * Add library dependencies to -dev packages

 -- Keith Packard <keithp@keithp.com>  Tue, 23 Oct 2018 09:50:25 -0700

cmark-gfm (0.28.3.gfm.19-2) unstable; urgency=medium

  * Add @builddeps@ to tests/control

 -- Keith Packard <keithp@keithp.com>  Mon, 22 Oct 2018 18:14:55 -0700

cmark-gfm (0.28.3.gfm.19-1) unstable; urgency=medium

  * Upstream version 0.28.3.gfm.19

 -- Keith Packard <keithp@keithp.com>  Mon, 22 Oct 2018 13:18:32 -0700

cmark-gfm (0.28.3.gfm.17-1) unstable; urgency=medium

  * Initial release. (Closes: #894271)

 -- Keith Packard <keithp@keithp.com>  Mon, 08 Oct 2018 11:08:01 -0700
